#include <iostream>

using namespace std;

class A {
public:
	int a;
	int Afoo(int* ip) { return *ip; }
	int Abar(double (*f)(float &)) {
		float d = 1.23;
		return f(d) == 1.23 ? 1 : 0;
	}
};

int foo() { return 0; }

char *bar(char* a, char* b) {
	return a;
}


void cat(void(*f)()) {
	return f();
}


int main() {
	// int *
	int i = 5;
	int *ip = &i;

	// int &
	int &ir(i);

	// double
	double d = 1.23;

	// A *
	A* a = new A;

	// char const *
	char c = 'c';
	char const *cp = &c;

	// char const &
	char const &cr(c);

	// long[7]
	long lArray[7] = { 1, 2, 3, 4, 5, 6, 7 };

	// int **
	int ** ipp = &ip;

	// int *&
	int *& ipr(ip);

	// float &
	float f = 0.123;
	float &fr(f);

	// int (*)()
	int (*pfunction) () = &foo;

	// int (*&)()
	int(*& rfunction) () = pfunction;

	// char *(*)(char*, char*)
	char *(*char_ptr_pfunction) (char *, char*) = &bar;

	// int A::*
	int A::*aptr = &A::a;

	// int (A::*) (int *)
	int (A::* Afoo_ptr) (int *) = &A::Afoo;

	// int (A::**) (int *)
	int (A::** Afoo_ptr_ptr) (int *) = &Afoo_ptr;

	// int (A::*&) (int*)
	int (A::*& Afoo_ptr_ref) (int*) = Afoo_ptr;

	// int (A::*) (double (*) (float &))
	int (A::* Abar_ptr) (double(*) (float &)) = &A::Abar;

	// void (*p[10]) (void (*)())
	void (*p[10]) (void(*)()) = { NULL };
	for (int k = 0; k < 10; k++) {
		p[k] = &cat;
	}

}