#include <iostream>
#include <ctime>
#include <cstdlib>

using namespace std;

// a class that collect the grades of the students
class Grades {
public:
	int count;		// number of grades
	int *grades;	// grades
	Grades() {
		count = 0;
		grades = new int[60];
	}

	void append(int grade) {
		if (count < 60) {
			grades[count] = grade;
			count++;
		}
	}

	// given a grade *ip, calculate how many students now in the current Grades class have a higher grade than the given grade 
	int ranking(int* ip) {
		int res = 0;
		for (int i = 0; i < count; i++)
			if (grades[i] > *ip)
				res++;
		return res;
	}


	// calculate the ranking of the average grade
	int ranking_average(int(*f)(float &)) {
		int total = 0;
		float average;
		for (int i = 0; i < count; i++)
			total += grades[i];
		average = total / count;

		int average_int = f(average);
		return ranking(&average_int);
	}
};

// full credits
int fullCredits() { return 100; }

// compare two grades
int *max(int* a, int* b) {
	return *a > *b ? a : b;
}

int float_to_int(float& d) {
	int di = (int)d;
	return di;
}

int main() {
	// int *
	int newGrade = 80;
	int *newGrade_ptr = &newGrade;
	cout << "newGrade_ptr: " << newGrade_ptr << endl;


	// int &
	int &newGrade_ref(newGrade);
	cout << "newGrade_ref: " << newGrade_ref << endl;

	// A *
	Grades* grades = new Grades;

	// initialize the grades
	srand(unsigned(time(0)));
	for (int i = 0; i < 50; i++) {
		int grade = rand() % 40 + 60;
		grades->append(grade);
	}
	cout << "Now there are " << grades->count << " grades" << endl;

	// char const *
	char level = 'A';
	char const *level_ptr = &level;
	cout << "get level using level_ptr: " << *level_ptr << endl;

	// char const &
	char const &level_ref(level);
	cout << "get level using level_ref: " << level_ref << endl;

	// int[10]
	int gradeArray[10] = { 100, 95, 90, 85, 80, 75, 70, 65, 60, 55};
	for (int i = 0; i < 10; i++)
		cout << gradeArray[i] << "  ";
	cout << endl;

	// int **
	int ** newGrade_ptr_ptr = &newGrade_ptr;
	cout << "newGrade_ptr_ptr: " << newGrade_ptr_ptr << endl;

	// int *&
	int *& newGrade_ptr_ref(newGrade_ptr);
	cout << "newGrade_ptr_ref: " << newGrade_ptr_ref << endl;

	// float &
	float gradeArray_average;
	int temp = 0;
	for (int i = 0; i < 10; i++)
		temp += gradeArray[i];
	gradeArray_average = temp / 10;
	float &gradeArray_average_ref(gradeArray_average);
	cout << "get average using gradeArray_average_ref: " << gradeArray_average_ref << endl;

	// int (*)()
	int(*fullCredits_ptr) () = &fullCredits;
	cout << "get full credits using fullCredits_ptr: " << fullCredits_ptr() << endl;

	// int (*&)()
	int(*& fullCredits_ptr_ref) () = fullCredits_ptr;
	cout << "get full credits using fullCredits_ptr_ref: " << fullCredits_ptr_ref() << endl;

	// int *(*)(int*, int*)
	int *(*max_ptr)(int*, int*) = &max;
	cout << "get max using max_ptr: " << *max_ptr(&gradeArray[0], &gradeArray[1]) << endl;

	// int A::*
	int Grades::*count_ptr = &Grades::count;
	cout << "get grades count using Grades::count_ptr: " << grades->*count_ptr << endl;

	// int (A::*) (int *)
	int (Grades::*ranking_ptr) (int *) = &Grades::ranking;
	cout << "get grades ranking Grades::ranking_ptr: " << (grades->*ranking_ptr)(newGrade_ptr) << endl;

	// int (A::**) (int *)
	int (Grades::** ranking_ptr_ptr) (int *) = &ranking_ptr;
	cout << "get grades ranking using ranking_ptr_ptr: " << (grades->**ranking_ptr_ptr)(newGrade_ptr) << endl;


	// int (A::*&) (int*)
	int (Grades::*& ranking_ptr_ref) (int*) = ranking_ptr;
	cout << "get grades ranking ranking_ptr_ref: " << (grades->*ranking_ptr_ref)(newGrade_ptr) << endl;

	// int (A::*) (double (*) (float &))
	int (Grades::* ranking_avearage_ptr) (int(*) (float &)) = &Grades::ranking_average;
	cout << "get grades average ranking using ranking_avearage_ptr: " << (grades->*ranking_avearage_ptr)(float_to_int) << endl;
	
}